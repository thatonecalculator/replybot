# ReplyBot

This is a Calckey/Misskey bot that will reply to any posts with given keywords built with [misskey-rs](https://docs.rs/misskey/latest/misskey/)

For self-hosting, copy `example.toml` to `config.toml`.