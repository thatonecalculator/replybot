use futures::stream::TryStreamExt;
use misskey::model::query::Query;
use misskey::prelude::*;
use misskey::WebSocketClient;
use serde::Deserialize;
use std::fs;
use toml;
use url::Url;

#[derive(Deserialize)]
struct Config {
    bot: Bot,
}

#[derive(Deserialize)]
struct Bot {
    api_token: String,
    server: String,
    words: Vec<String>,
    reply: String,
    case_sensitive: Option<bool>,
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    print!("Connecting to the Fediverse... ");

    let toml_str = fs::read_to_string("config.toml")?;
    let config: Config = toml::from_str(&toml_str).unwrap();
    let url = Url::parse(&config.bot.server).expect("Invalid server URL");

    let client = WebSocketClient::builder(url)
        .token(config.bot.api_token)
        .connect()
        .await?;

    let antenna = client
        .build_antenna()
        .name("word-reply")
        .include(Query::from_vec(
            config.bot.words.into_iter().map(|x| vec![x]).collect(),
        ))
        .case_sensitive(config.bot.case_sensitive.unwrap_or(false))
        .create()
        .await?;

    let mut stream = client.antenna_timeline(&antenna).await?;

    while let Some(note) = stream.try_next().await? {
        println!("Received a note from @{}", note.user.username);

        client.reply(&note, &config.bot.reply).await?;
    }

    Ok(())
}
